# TEXTOS



## Introducción

En este repositorio encontrarás textos divididos por categorías referidos a economía y finanzas. A medida que se vayan subiendo enlaces con nuevas categorías estas se irán indicando debajo.

## Generales

- [Manual Básico](https://gitlab.com/economiayfinanzas/textos/-/blob/main/Generales/ManualBasico.txt)
